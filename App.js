import React, { Component } from 'react';
import { Platform } from 'react-native';
import { Provider } from 'react-redux';
import { StackNavigator } from 'react-navigation';
import thunk from 'redux-thunk';
import Expo from 'expo';
import { Text } from 'native-base';
import { createStore, applyMiddleware } from 'redux';

import LoginView from './src/containers/LoginView';
import RegisterView from './src/containers/RegisterView';
import SetupView from './src/containers/SetupView';
import SelfLearnView from './src/containers/SelfLearnView';
import PromotionsView from './src/containers/PromotionsView';
import LotteryView from './src/containers/LotteryView';
import ReceiptsListView from './src/containers/ReceiptsListView';
import HomeView from './src/containers/HomeView';
import ClientPageView from './src/containers/ClientPageView';
import ScanReceiptView from './src/containers/ScanReceiptView';
import ReceiptView from './src/containers/ReceiptView';
import MissingReceiptView from './src/containers/MissingReceiptView';
import AddPromotionView from './src/containers/AddPromotionView';

import reducers from './src/reducers';

let store = createStore(reducers, applyMiddleware(thunk));

const Navigator = StackNavigator({
    Login: { screen: LoginView },
    Register: { screen: RegisterView },
    Setup: { screen: SetupView },
    SelfLearn: { screen: SelfLearnView },
    Promotions: { screen: PromotionsView },
    Lottery: { screen: LotteryView },
    Receipts: { screen: ReceiptsListView },
    Receipt: { screen: ReceiptView },
    Home: { screen: HomeView },
    Client: { screen: ClientPageView },
    ScanReceipt: { screen: ScanReceiptView },
    MissingReceipt: { screen: MissingReceiptView },
    AddPromotion: { screen: AddPromotionView },
}, {
    initialRouteName: 'Login',
    headerMode: 'none',
    mode: 'modal'
});

export default class App extends Component {
    constructor() {
        super();

        this.state = { isReady: false };
    }

    async componentWillMount() {
        await Expo.Font.loadAsync({
            'Roboto': require('native-base/Fonts/Roboto.ttf'),
            'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        });

        this.setState({ isReady: true });
    }

    render() {
        if (!this.state.isReady) return (<Text>Loading...</Text>);

        return (
            <Provider store={store}>
                <Navigator />
            </Provider>
        );
    }
}