package paragon;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import paragon.exceptions.IncorrectPasswordException;
import paragon.exceptions.PasswordNotMatchingException;
import paragon.exceptions.UserAlreadyExistsException;
import paragon.exceptions.UserNotAuthenticatedException;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandling {

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = EntityNotFoundException.class)
    @ResponseBody
    public Map<String, Object> entityNotFoundExceptionHandler(Exception ex) {
        Map<String, Object> result = new HashMap<>();
        result.put("error", ex.getMessage());
        return result;
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = PasswordNotMatchingException.class)
    @ResponseBody
    public Map<String, Object> passwordNotMatchingExceptionHandler(Exception ex) {
        Map<String, Object> result = new HashMap<>();
        result.put("error", ex.getMessage());
        return result;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = UserAlreadyExistsException.class)
    @ResponseBody
    public Map<String, Object> userAlreadyExistsExceptionHandler(Exception ex) {
        Map<String, Object> result = new HashMap<>();
        result.put("error", ex.getMessage());
        return result;
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {IncorrectPasswordException.class, UserNotAuthenticatedException.class})
    @ResponseBody
    public Map<String, Object> incorrectPasswordHandler(Exception ex) {
        Map<String, Object> result = new HashMap<>();
        result.put("error", ex.getMessage());
        return result;
    }

}
