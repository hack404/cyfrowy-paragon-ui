package paragon.controller;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import paragon.model.db.Promotion;
import paragon.model.db.User;
import paragon.model.dto.PromotionDTO;
import paragon.service.PromotionService;
import paragon.service.UserService;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */

@RestController
@RequestMapping(value = "/promotions")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PromotionController {

    @Autowired
    PromotionService promotionService;

    @Autowired
    UserService userService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<Promotion> getPromotion(@RequestHeader("Authentication") String authentication,@PathVariable("id") Long id) {
        userService.authenticate(authentication);
        return new ResponseEntity<>(promotionService.getPromotionById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Promotion> createPromotion(@RequestHeader("Authentication") String authentication, @RequestBody PromotionDTO promotionDTO) {
        User user = userService.authenticate(authentication);
        return new ResponseEntity<>(promotionService.createPromotion(user, promotionDTO), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Promotion> updatePromotion(@RequestHeader("Authentication") String authentication,
                                                     @PathParam("id") Long promotionId,
                                                     @RequestBody PromotionDTO promotionDTO) {
        userService.authenticate(authentication);
        return new ResponseEntity<>(promotionService.updatePromotion(promotionId, promotionDTO), HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<PromotionDTO>> getAllUserPromotions(@RequestHeader("Authentication") String authentication) {
        User user = userService.authenticate(authentication);
        return new ResponseEntity<>(promotionService.getAllPromotions(user), HttpStatus.OK);
    }

    @PostMapping(path = "/{id}/image")
    public ResponseEntity<?> uploadImage(@PathVariable("id") Long id,
                                         @RequestParam("file") MultipartFile file,
                                         @RequestHeader("Authentication") String authentication) {
        userService.authenticate(authentication);
        promotionService.uploadImage(id, file);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(path = "/{id}/image")
    public void getImage(@PathVariable("id") Long id,
                         HttpServletResponse response) throws IOException {
        InputStream in = promotionService.getPromotionImage(id);
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }
}
