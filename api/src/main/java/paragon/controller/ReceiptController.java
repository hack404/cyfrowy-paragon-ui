package paragon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import paragon.model.db.MissingReceipt;
import paragon.model.db.Receipt;
import paragon.model.db.User;
import paragon.model.dto.MissingReceiptDTO;
import paragon.model.dto.ReceiptInDTO;
import paragon.model.dto.ReceiptOutDTO;
import paragon.service.ReceiptService;
import paragon.service.UserService;

import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */

@RestController
@RequestMapping(value = "/receipts")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ReceiptController {

    @Autowired
    ReceiptService receiptService;

    @Autowired
    UserService userService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<ReceiptOutDTO> getReceipt(@RequestHeader("Authentication") String authentication,@PathVariable("id") Long id) {
        userService.authenticate(authentication);
        return new ResponseEntity<>(receiptService.getReceiptById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Receipt> createReceipt(@RequestHeader("Authentication") String authentication, @RequestBody ReceiptInDTO receiptInDTO) {
        User user = userService.authenticate(authentication);
        return new ResponseEntity<>(receiptService.createReceipt(receiptInDTO, user), HttpStatus.OK);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<ReceiptOutDTO>> getAllUserReceipts(@RequestHeader("Authentication") String authentication) {
        User user = userService.authenticate(authentication);
        return new ResponseEntity<>(receiptService.getAllUserReceipts(user), HttpStatus.OK);
    }

    @PostMapping(value = "/missing")
    public ResponseEntity<MissingReceipt> createMissingReceipt(@RequestHeader("Authentication") String authentication, @RequestBody MissingReceiptDTO missingReceiptDTO) {
        User user = userService.authenticate(authentication);
        return new ResponseEntity<>(receiptService.createMissingReceipt(missingReceiptDTO, user), HttpStatus.OK);
    }
}
