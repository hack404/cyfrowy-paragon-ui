package paragon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import paragon.model.db.Shop;
import paragon.service.ShopService;
import paragon.service.UserService;

/**
 * Created by 44072401 on 10/28/17.
 */

@RestController
@RequestMapping(value = "/shops")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ShopController {

    @Autowired
    ShopService shopService;

    @Autowired
    UserService userService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<Shop> getShop(@RequestHeader("Authentication") String authentication, @PathVariable("id") Long id) {
        userService.authenticate(authentication);

        return new ResponseEntity<>(shopService.getShopById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Shop> createShop(@RequestHeader("Authentication") String authentication, @RequestBody Shop shop) {
        return new ResponseEntity<>(shopService.createShop(shop), HttpStatus.OK);
    }
}
