package paragon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import paragon.model.dto.ShopDTO;
import paragon.model.dto.ShopStatisticsDTO;
import paragon.model.dto.UserShopDistancesDTO;
import paragon.service.StatisticsService;
import paragon.service.UserService;

import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/stats")
public class StatisticsController {

    @Autowired
    private UserService userService;

    @Autowired
    private StatisticsService statisticsService;

    @GetMapping(value = "/receipt-missing-shops")
    public ResponseEntity<List<ShopDTO>> getMissingReceiptShops() {
        return new ResponseEntity<>(statisticsService.getReceiptMissingShops(), HttpStatus.OK);
    }

    @GetMapping(value = "/users-distance")
    public ResponseEntity<List<UserShopDistancesDTO>> getUsersReaches() {
        return new ResponseEntity<>(statisticsService.getUsersDistances(), HttpStatus.OK);
    }

    @GetMapping(value = "/shops")
    public ResponseEntity<List<ShopStatisticsDTO>> getShopStatistics(@RequestParam(value = "sector", required = false) String sector) {
        return new ResponseEntity<>(statisticsService.getShopsStatistics(sector), HttpStatus.OK);
    }

    @GetMapping(value = "/shops/missing-receipt-user")
    public ResponseEntity<List<ShopDTO>> getShopsLackingReceiptUser() {
        return new ResponseEntity<>(statisticsService.getShopsLackingReceiptUser(), HttpStatus.OK);
    }

    @GetMapping(value = "/shops/missing-receipt-total")
    public ResponseEntity<List<ShopDTO>> getShopsLackingReceiptTotal() {
        return new ResponseEntity<>(statisticsService.getShopsLackingReceiptTotal(), HttpStatus.OK);
    }
}
