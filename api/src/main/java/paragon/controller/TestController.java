package paragon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import paragon.service.TestDataService;

/**
 * Created by 44072401 on 10/29/17.
 */
@RestController
@RequestMapping(value = "/test")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TestController {

    @Autowired
    TestDataService testDataService;

    @GetMapping
    public ResponseEntity<?> createTestData() {
        testDataService.createTestData();
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
