package paragon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import paragon.model.Token;
import paragon.model.dto.UserDTO;
import paragon.service.UserService;

/**
 * Created by 44072401 on 10/28/17.
 */
@RestController
@RequestMapping(value = "/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(path = "/login")
    public ResponseEntity<Token> login(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(userService.login(userDTO), HttpStatus.OK);
    }

    @PostMapping(path = "/logout")
    public ResponseEntity<?> logout(@RequestBody UserDTO userDTO) {
        userService.logout(userDTO);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    public ResponseEntity<?> registerUser(@RequestBody UserDTO userDTO) {
        userService.registerUser(userDTO);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
