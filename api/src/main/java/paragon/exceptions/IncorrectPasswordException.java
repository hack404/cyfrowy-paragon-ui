package paragon.exceptions;

/**
 * Created by 44072401 on 10/28/17.
 */
public class IncorrectPasswordException extends RuntimeException {
    public IncorrectPasswordException(String message) {
        super(message);
    }
}
