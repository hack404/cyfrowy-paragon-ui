package paragon.exceptions;

/**
 * Created by 44072401 on 10/28/17.
 */
public class PasswordNotMatchingException extends RuntimeException {
    public PasswordNotMatchingException(String message) {
        super(message);
    }
}
