package paragon.exceptions;

/**
 * Created by 44072401 on 10/28/17.
 */
public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException(String message) {
        super(message);
    }
}
