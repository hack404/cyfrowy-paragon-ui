package paragon.exceptions;

/**
 * Created by 44072401 on 10/28/17.
 */
public class UserNotAuthenticatedException extends RuntimeException {
    public UserNotAuthenticatedException() {
    }

    public UserNotAuthenticatedException(String message) {
        super(message);
    }
}
