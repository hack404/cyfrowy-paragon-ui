package paragon.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by 44072401 on 10/28/17.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Token {
    private String token;
}
