package paragon.model.db;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by 44072401 on 10/28/17.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Promotion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    @Lob
    private byte[] photo;
    private Long startDate;
    private Long endDate;
    @Enumerated(EnumType.STRING)
    private PromotionTarget target;
    @Enumerated(EnumType.STRING)
    private PromotionStatus status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shop_id")
    private Shop shop;
}
