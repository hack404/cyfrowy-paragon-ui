package paragon.model.db;

/**
 * Created by 44072401 on 10/28/17.
 */
public enum PromotionStatus {
    ON_HOLD,
    ACTIVE
}
