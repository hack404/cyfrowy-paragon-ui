package paragon.model.db;

/**
 * Created by 44072401 on 10/28/17.
 */
public enum PromotionTarget {
    LAST_THREE_DAYS_MAX,
    LAST_MONTH_MOST_FREQUENT,
    LAST_YEAR_LEST_FREQUENT
}
