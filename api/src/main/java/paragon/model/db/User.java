package paragon.model.db;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by 44072401 on 10/28/17.
 */

@Entity
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String surname;

    private String email;

    private String password;

    @Enumerated(EnumType.STRING)
    private UserType type;

    private String nip;

    private String token;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shop_id")
    private Shop shop;
}
