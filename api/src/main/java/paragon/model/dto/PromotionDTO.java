package paragon.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import paragon.model.db.PromotionStatus;
import paragon.model.db.PromotionTarget;

/**
 * Created by 44072401 on 10/29/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionDTO {
    private Long id;
    private String name;
    private String description;
    private Long startDate;
    private Long endDate;
    private PromotionTarget target;
    private PromotionStatus status;
    private String imgUrl;
    private String shopName;
}
