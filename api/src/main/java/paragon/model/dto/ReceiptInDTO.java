package paragon.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import paragon.model.db.Product;

import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ReceiptInDTO {

    private String shopAddress;

    private String shopName;

    private String date;

    private String time;

    List<Product> products;
}
