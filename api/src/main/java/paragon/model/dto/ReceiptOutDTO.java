package paragon.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import paragon.model.db.ShopSector;

/**
 * Created by 44072401 on 10/28/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ReceiptOutDTO {

    private String shopName;
    private String shopAddress;
    private Long date;
    private Double totalPrice;
    private ShopSector sector;

}
