package paragon.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import paragon.model.db.ShopSector;

/**
 * Created by 44072401 on 10/28/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopDTO {

    private String name;

    private String address;

    private String city;

    private Double latitude;

    private Double longitude;

    private ShopSector sector;

    private String color;
}
