package paragon.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by 44072401 on 10/29/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShopStatisticsDTO {

    private String name;

    private String address;

    private String city;

    private Integer receiptsNumber;

    private Integer usersNumber;

    private Double latitude;

    private Double longitude;

    private String color;

}
