package paragon.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import paragon.model.db.UserType;

/**
 * Created by 44072401 on 10/28/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {
    private String name;

    private String surname;

    private String email;

    private String password;

    private String matchingPassword;

    private String nip;

    private UserType type;

    private Long shopId;
}
