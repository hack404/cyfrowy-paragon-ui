package paragon.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by 44072401 on 10/28/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserShopDistancesDTO {
    Double longitude;
    Double latitude;
    Double distance;
}
