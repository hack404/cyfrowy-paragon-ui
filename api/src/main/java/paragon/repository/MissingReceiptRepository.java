package paragon.repository;

import org.springframework.data.repository.CrudRepository;
import paragon.model.db.MissingReceipt;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface MissingReceiptRepository extends CrudRepository<MissingReceipt, Long> {

}
