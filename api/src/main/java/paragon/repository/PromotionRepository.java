package paragon.repository;

import org.springframework.data.repository.CrudRepository;
import paragon.model.db.Promotion;
import paragon.model.db.Shop;

import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface PromotionRepository extends CrudRepository<Promotion, Long> {
    List<Promotion> findAllByShop(Shop shop);
}
