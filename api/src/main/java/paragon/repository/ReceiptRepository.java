package paragon.repository;

import org.springframework.data.repository.CrudRepository;
import paragon.model.db.Receipt;
import paragon.model.db.User;

import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface ReceiptRepository extends CrudRepository<Receipt, Long> {
    List<Receipt> findAllByUser(User user);
}
