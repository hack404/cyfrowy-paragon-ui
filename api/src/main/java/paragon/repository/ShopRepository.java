package paragon.repository;

import org.springframework.data.repository.CrudRepository;
import paragon.model.db.Shop;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface ShopRepository extends CrudRepository<Shop, Long> {
    Shop findFirstByNameAndAddress(String name, String address);

    Shop findFirstByName(String name);
}
