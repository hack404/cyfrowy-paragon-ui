package paragon.repository;

import org.springframework.data.repository.CrudRepository;
import paragon.model.db.User;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findFirstByEmail(String email);
}
