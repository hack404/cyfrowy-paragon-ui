package paragon.service;

import org.springframework.web.multipart.MultipartFile;
import paragon.model.db.Promotion;
import paragon.model.db.User;
import paragon.model.dto.PromotionDTO;

import java.io.InputStream;
import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface PromotionService {
    Promotion createPromotion(User user, PromotionDTO promotionDTO);

    Promotion updatePromotion(Long id, PromotionDTO promotion);

    Promotion getPromotionById(Long id);

    List<PromotionDTO> getAllPromotions(User user);

    void uploadImage(Long promotionId, MultipartFile file);

    InputStream getPromotionImage(Long promotionId);
}
