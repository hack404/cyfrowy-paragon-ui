package paragon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import paragon.model.db.Promotion;
import paragon.model.db.User;
import paragon.model.dto.PromotionDTO;
import paragon.repository.PromotionRepository;

import javax.persistence.EntityNotFoundException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by 44072401 on 10/28/17.
 */
@Service
public class PromotionServiceImpl implements PromotionService {

    @Autowired
    PromotionRepository promotionRepository;

    @Override
    public Promotion createPromotion(User user, PromotionDTO promotionDTO) {
        Promotion promotion = new Promotion();
        promotion.setTarget(promotionDTO.getTarget());
        promotion.setStatus(promotionDTO.getStatus());
        promotion.setName(promotionDTO.getName());
        promotion.setStartDate(promotionDTO.getStartDate());
        promotion.setEndDate(promotionDTO.getEndDate());
        promotion.setDescription(promotionDTO.getDescription());
        promotion.setShop(user.getShop());

        return promotionRepository.save(promotion);
    }

    @Override
    public Promotion updatePromotion(Long id, PromotionDTO promotionDTO) {
        return Optional.ofNullable(promotionRepository.findOne(id)).map(s -> {
            s.setDescription(promotionDTO.getDescription());
            s.setEndDate(promotionDTO.getEndDate());
            s.setStartDate(promotionDTO.getStartDate());
            s.setName(promotionDTO.getName());
            s.setStatus(promotionDTO.getStatus());
            s.setTarget(promotionDTO.getTarget());
            return promotionRepository.save(s);
        }).orElseThrow(() -> new EntityNotFoundException("Promotion not found"));
    }

    @Override
    public Promotion getPromotionById(Long id) {
        return Optional.ofNullable(promotionRepository.findOne(id))
                .orElseThrow(() -> new EntityNotFoundException("Promotion not found"));
    }

    @Override
    public List<PromotionDTO> getAllPromotions(User user) {
        List<PromotionDTO> promotions = new ArrayList<>();
        promotionRepository.findAllByShop(user.getShop()).forEach(s -> {
            PromotionDTO promotionDTO = new PromotionDTO();
            promotionDTO.setId(s.getId());
            promotionDTO.setDescription(s.getDescription());
            promotionDTO.setEndDate(s.getEndDate());
            promotionDTO.setStartDate(s.getStartDate());
            promotionDTO.setName(s.getName());
            promotionDTO.setStatus(s.getStatus());
            promotionDTO.setTarget(s.getTarget());
            promotionDTO.setShopName(user.getShop().getName());
            promotions.add(promotionDTO);
        });

        return promotions;
    }

    @Override
    public void uploadImage(Long promotionId, MultipartFile file) {
        Promotion promotion = promotionRepository.findOne(promotionId);
        try {
            promotion.setPhoto(file.getBytes());
            promotionRepository.save(promotion);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public InputStream getPromotionImage(Long promotionId) {
        Promotion promotion = promotionRepository.findOne(promotionId);
        return new ByteArrayInputStream(promotion.getPhoto());
    }
}
