package paragon.service;

import paragon.model.db.MissingReceipt;
import paragon.model.db.Receipt;
import paragon.model.db.User;
import paragon.model.dto.MissingReceiptDTO;
import paragon.model.dto.ReceiptInDTO;
import paragon.model.dto.ReceiptOutDTO;

import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface ReceiptService {
    Receipt createReceipt(ReceiptInDTO receiptInDTO, User user);

    ReceiptOutDTO getReceiptById(Long id);

    List<ReceiptOutDTO> getAllUserReceipts(User user);

    MissingReceipt createMissingReceipt(MissingReceiptDTO missingReceiptDTO, User user);
}
