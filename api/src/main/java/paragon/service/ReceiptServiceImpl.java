package paragon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import paragon.model.db.MissingReceipt;
import paragon.model.db.Receipt;
import paragon.model.db.Shop;
import paragon.model.db.User;
import paragon.model.dto.MissingReceiptDTO;
import paragon.model.dto.ReceiptInDTO;
import paragon.model.dto.ReceiptOutDTO;
import paragon.repository.MissingReceiptRepository;
import paragon.repository.ReceiptRepository;
import paragon.repository.ShopRepository;

import javax.persistence.EntityNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by 44072401 on 10/28/17.
 */

@Service
public class ReceiptServiceImpl implements ReceiptService {

    @Autowired
    private ReceiptRepository receiptRepository;

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private MissingReceiptRepository missingReceiptRepository;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    @Override
    public Receipt createReceipt(ReceiptInDTO receiptInDTO, User user) {
        Shop shop = Optional.ofNullable(shopRepository.findFirstByNameAndAddress(receiptInDTO.getShopName(), receiptInDTO.getShopAddress()))
                .orElseThrow(() -> new EntityNotFoundException("Could not find shop for this receipt"));

        Receipt receipt = new Receipt();
        receipt.setUser(user);
        String dateCombined = receiptInDTO.getDate() + " " + receiptInDTO.getTime();
        try {
            receipt.setDateStored(dateFormat.parse(dateCombined).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        receipt.setProducts(receiptInDTO.getProducts());
        receipt.setShop(shop);
        return receiptRepository.save(receipt);
    }

    @Override
    public ReceiptOutDTO getReceiptById(Long id) {
        return Optional.ofNullable(receiptRepository.findOne(id)).map(s -> {
            ReceiptOutDTO receiptOutDTO = new ReceiptOutDTO();
            receiptOutDTO.setSector(s.getShop().getSector());
            receiptOutDTO.setShopName(s.getShop().getName());
            receiptOutDTO.setShopAddress(s.getShop().getAddress());
            receiptOutDTO.setDate(s.getDateStored());
            receiptOutDTO.setTotalPrice(0.0);
            s.getProducts().forEach(p -> {
                receiptOutDTO.setTotalPrice(receiptOutDTO.getTotalPrice() + p.getQuantity() * p.getPrice());
            });
            return receiptOutDTO;
        }).orElseThrow(() -> new EntityNotFoundException("Receipt not found"));
    }

    @Override
    public List<ReceiptOutDTO> getAllUserReceipts(User user) {
        List<ReceiptOutDTO> receipts = new ArrayList<>();
        receiptRepository.findAllByUser(user).forEach(s -> {
            ReceiptOutDTO receiptOutDTO = new ReceiptOutDTO();

            receiptOutDTO.setTotalPrice(0.0);
            s.getProducts().forEach(p -> {
                receiptOutDTO.setTotalPrice(receiptOutDTO.getTotalPrice() + p.getPrice() * p.getQuantity());
            });

            receiptOutDTO.setDate(s.getDateStored());
            receiptOutDTO.setShopAddress(s.getShop().getAddress());
            receiptOutDTO.setShopName(s.getShop().getName());
            receiptOutDTO.setSector(s.getShop().getSector());
            receipts.add(receiptOutDTO);
        });
        return receipts;
    }

    @Override
    public MissingReceipt createMissingReceipt(MissingReceiptDTO missingReceiptDTO, User user) {
        MissingReceipt missingReceipt = new MissingReceipt();

        Shop shop = Optional.ofNullable(shopRepository.findFirstByName(missingReceiptDTO.getShopName()))
                .orElseThrow(() -> new EntityNotFoundException("Could not find show with this name"));


        missingReceipt.setDate(new Date().getTime());
        missingReceipt.setShop(shop);
        missingReceipt.setUser(user);

        return missingReceiptRepository.save(missingReceipt);
    }
}
