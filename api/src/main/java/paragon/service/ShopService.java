package paragon.service;

import paragon.model.db.Shop;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface ShopService {
    Shop createShop(Shop shop);

    Shop getShopById(Long id);
}
