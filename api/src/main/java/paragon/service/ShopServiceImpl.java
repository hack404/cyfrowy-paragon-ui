package paragon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import paragon.model.db.Shop;
import paragon.repository.ShopRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * Created by 44072401 on 10/28/17.
 */
@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    ShopRepository shopRepository;

    @Override
    public Shop createShop(Shop shop) {
        return shopRepository.save(shop);
    }

    @Override
    public Shop getShopById(Long id) {
        return Optional.ofNullable(shopRepository.findOne(id)).orElseThrow(() -> new EntityNotFoundException("Shop with this id does not exist"));
    }
}
