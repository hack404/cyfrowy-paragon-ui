package paragon.service;

import paragon.model.dto.ShopDTO;
import paragon.model.dto.ShopStatisticsDTO;
import paragon.model.dto.UserShopDistancesDTO;

import java.util.List;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface StatisticsService {
    List<ShopDTO> getReceiptMissingShops();

    List<UserShopDistancesDTO> getUsersDistances();

    List<ShopStatisticsDTO> getShopsStatistics(String sector);

    List<ShopDTO> getShopsLackingReceiptUser();

    List<ShopDTO> getShopsLackingReceiptTotal();
}
