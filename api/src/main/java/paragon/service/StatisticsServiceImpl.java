package paragon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import paragon.model.db.*;
import paragon.model.dto.ShopDTO;
import paragon.model.dto.ShopStatisticsDTO;
import paragon.model.dto.UserShopDistancesDTO;
import paragon.repository.MissingReceiptRepository;
import paragon.repository.ReceiptRepository;
import paragon.repository.UserRepository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by 44072401 on 10/28/17.
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    MissingReceiptRepository missingReceiptRepository;

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<ShopDTO> getReceiptMissingShops() {
        List<ShopDTO> missingReceiptsShops = new ArrayList<>();
        missingReceiptRepository.findAll().forEach(s -> {
            ShopDTO shopDTO = new ShopDTO();
            shopDTO.setAddress(s.getShop().getAddress());
            shopDTO.setCity(s.getShop().getCity());
            shopDTO.setName(s.getShop().getName());
            shopDTO.setSector(s.getShop().getSector());
            missingReceiptsShops.add(shopDTO);
        });

        return missingReceiptsShops;
    }

    @Override
    public List<UserShopDistancesDTO> getUsersDistances() {
        List<UserShopDistancesDTO> distances = new ArrayList<>();

        for (User user : userRepository.findAll()) {
            double sumLatitude = 0.0;
            double sumLongitude = 0.0;

            int receiptsNum = 0;

            boolean userHasReceipt = false;

            for (Receipt receipt : receiptRepository.findAllByUser(user)) {
                userHasReceipt = true;
                sumLatitude += receipt.getShop().getLatitude();
                sumLongitude += receipt.getShop().getLongitude();
                receiptsNum++;
            }

            if (userHasReceipt == false) {
                continue;
            }

            double averageLatitude = sumLatitude / receiptsNum;
            double averageLongitude = sumLongitude / receiptsNum;

            double maxDistance = 0.0;

            for (Receipt receipt : receiptRepository.findAllByUser(user)) {
                double latitudeDist = receipt.getShop().getLatitude() - averageLatitude;
                double longitudeDist = receipt.getShop().getLongitude() - averageLongitude;

                double distance = Math.sqrt(Math.pow(latitudeDist, 2) + Math.pow(longitudeDist, 2));

                maxDistance = distance > maxDistance ? distance : maxDistance;
            }

            UserShopDistancesDTO userShopDistancesDTO = new UserShopDistancesDTO();
            userShopDistancesDTO.setLatitude(averageLatitude);
            userShopDistancesDTO.setLongitude(averageLongitude);
            userShopDistancesDTO.setDistance(maxDistance);

            distances.add(userShopDistancesDTO);
        }

        return distances;
    }

    @Override
    public List<ShopStatisticsDTO> getShopsStatistics(String sector) {
        Random random = new Random();

        ShopSector shopSector = Optional.ofNullable(sector)
                .map(s -> ShopSector.valueOf(sector.toUpperCase()))
                .orElse(null);

        List<ShopStatisticsDTO> shopStatisticsDTOS = new ArrayList<>();

        Map<Shop, Integer> shopReceipts = new HashMap<>();

        Map<Shop, Set<User>> shopUsers = new HashMap<>();

        for (Receipt receipt : receiptRepository.findAll()) {
            if (shopSector != null && shopSector != receipt.getShop().getSector()) {
                continue;
            }

            shopReceipts.merge(receipt.getShop(), 1, (oldVal, newVal) -> oldVal + newVal);

            Set<User> users = shopUsers.getOrDefault(receipt.getShop(), new HashSet<>());

            users.add(receipt.getUser());

            shopUsers.put(receipt.getShop(), users);
        }

        for (Shop shop : shopReceipts.keySet()) {
            ShopStatisticsDTO shopStatisticsDTO = new ShopStatisticsDTO();
            shopStatisticsDTO.setReceiptsNumber(shopReceipts.get(shop));
            shopStatisticsDTO.setUsersNumber(shopUsers.get(shop).size());

            shopStatisticsDTO.setAddress(shop.getAddress());
            shopStatisticsDTO.setCity(shop.getCity());
            shopStatisticsDTO.setName(shop.getName());
            shopStatisticsDTO.setLatitude(shop.getLatitude());
            shopStatisticsDTO.setLongitude(shop.getLongitude());


            shopStatisticsDTO.setColor("("
                    + random.nextInt(255) + ","
                    + random.nextInt(255) + ","
                    + random.nextInt(255) + ")"
            );

            shopStatisticsDTOS.add(shopStatisticsDTO);
        }

        return shopStatisticsDTOS;
    }

    public List<ShopDTO> getShopsLackingReceiptUser() {
        Random random = new Random();

        Map<Shop, Set<User>> shopSetMap = new HashMap<>();

        for (MissingReceipt missingReceipt : missingReceiptRepository.findAll()) {
            Set<User> users = shopSetMap.getOrDefault(missingReceipt.getShop(), new HashSet<>());

            users.add(missingReceipt.getUser());

            shopSetMap.put(missingReceipt.getShop(), users);
        }

        Map<Shop, Set<User>> sortedByMaximumUsersWithoutReceipt = shopSetMap.entrySet().stream()
                .sorted((o1, o2) -> {
                    if(o2.getValue().size() > o1.getValue().size()){
                        return 1;
                    } else if(o2.getValue().size() < o1.getValue().size()){
                        return -1;
                    } else {
                        return 0;
                    }
                })
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return sortedByMaximumUsersWithoutReceipt.keySet().stream()
                .map(s -> {
                    ShopDTO shopDTO = new ShopDTO();
                    shopDTO.setAddress(s.getAddress());
                    shopDTO.setCity(s.getCity());
                    shopDTO.setName(s.getName());
                    shopDTO.setSector(s.getSector());
                    shopDTO.setLatitude(s.getLatitude());
                    shopDTO.setLongitude(s.getLongitude());
                    shopDTO.setColor("("
                            + random.nextInt(255) + ","
                            + random.nextInt(255) + ","
                            + random.nextInt(255) + ")"
                    );;
                    return shopDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<ShopDTO> getShopsLackingReceiptTotal() {
        Random random = new Random();

        Map<Shop, Integer> shopSetMap = new HashMap<>();

        for (MissingReceipt missingReceipt : missingReceiptRepository.findAll()) {
            shopSetMap.merge(missingReceipt.getShop(), 1, (oldVal, newVal) -> oldVal + newVal);
        }

        Map<Shop, Integer> sorted = shopSetMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return sorted.keySet().stream()
                .map(s -> {
                    ShopDTO shopDTO = new ShopDTO();
                    shopDTO.setAddress(s.getAddress());
                    shopDTO.setCity(s.getCity());
                    shopDTO.setName(s.getName());
                    shopDTO.setSector(s.getSector());
                    shopDTO.setLatitude(s.getLatitude());
                    shopDTO.setLongitude(s.getLongitude());
                    shopDTO.setColor("("
                            + random.nextInt(255) + ","
                            + random.nextInt(255) + ","
                            + random.nextInt(255) + ")"
                    );
                    return shopDTO;
                })
                .collect(Collectors.toList());
    }
}
