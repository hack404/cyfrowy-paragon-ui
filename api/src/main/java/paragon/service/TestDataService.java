package paragon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import paragon.model.db.*;
import paragon.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by 44072401 on 10/29/17.
 */
@Service
public class TestDataService {

    @Autowired
    ShopRepository shopRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    MissingReceiptRepository missingReceiptRepository;

    @Autowired
    PromotionRepository promotionRepository;

    //Shops
    public static final Integer SHOPS_NUMBER = 100;

    public static final Double LATITUDE_MIN = 50.6;
    public static final Double LATITUDE_MAX = 53.95;

    public static final Double LONGITUDE_MIN = 16.0;
    public static final Double LONGITUDE_MAX = 22.1;

    public static final String SHOP_NAME = "Beautiful Shop ";
    public static final String SHOP_ADDRESS = "ul. Krasnobrodzka ";
    public static final String SHOP_CITY = "Kraków ";

    //Users
    public static final Integer USERS_NUMBER = 30;

    public static final String USER_EMAIL_PREFIX = "jan.kowalski";
    public static final String USER_EMAIL_POSTFIX = "@gmail.com";
    public static final String USER_NAME = "Jan";
    public static final String USER_SURNAME = "Kowalski";
    public static final String USER_PASSWORD = "12345";
    public static final String USER_NIP = "111111111111";

    //Receipts
    public static final String PRODUCT_NAME = "Biurko ";
    public static final Long RECEIPT_DATE_MIN = 1508450400000l;
    public static final Long RECEIPT_DATE_RANGE = 867600000l;

    //Missing receipts
    public static final Integer MISSING_RECEIPTS_NUMBER = 100;

    //Promotions
    public static final Integer PROMOTIONS_NUMBER = 20;
    public static final String PROMOTION_DESCRIPTION = "30% bonus";
    public static final String PROMOTION_NAME = "Get your free 30% bonus";


    private List<Shop> createdShops = new ArrayList<>();
    private List<User> createdUsers = new ArrayList<>();


    public void createTestShops() {
        for(int i = 0; i < SHOPS_NUMBER; i++) {
            Shop shop = new Shop();
            shop.setAddress(SHOP_ADDRESS + String.valueOf(i));
            shop.setName(SHOP_NAME + String.valueOf(i));
            shop.setLatitude(LATITUDE_MIN + Math.random()*(LATITUDE_MAX - LATITUDE_MIN));
            shop.setLongitude(LONGITUDE_MIN + Math.random()*(LONGITUDE_MAX - LONGITUDE_MIN));
            shop.setCity(SHOP_CITY + String.valueOf(i));
            shop.setSector(ShopSector.values()[new Random().nextInt(3)]);

            createdShops.add(shopRepository.save(shop));
        }
    }

    public void createTestUsers() {
        //create users with shops
        int i = 0;
        for(; i < 10; i++) {
            User user = new User();
            user.setNip(USER_NIP + String.valueOf(i));
            user.setType(UserType.FIRMA);
            user.setEmail(USER_EMAIL_PREFIX + String.valueOf(i) + USER_EMAIL_POSTFIX);
            user.setName(USER_NAME + String.valueOf(i));
            user.setPassword(USER_PASSWORD);
            user.setSurname(USER_SURNAME + String.valueOf(i));
            user.setShop(createdShops.get(i));

            createdUsers.add(userRepository.save(user));
        }

        for(; i < USERS_NUMBER; i++) {
            User user = new User();
            user.setType(UserType.KLIENT);
            user.setEmail(USER_EMAIL_PREFIX + String.valueOf(i) + USER_EMAIL_POSTFIX);
            user.setName(USER_NAME + String.valueOf(i));
            user.setPassword(USER_PASSWORD);
            user.setSurname(USER_SURNAME + String.valueOf(i));

            createdUsers.add(userRepository.save(user));
        }
    }

    public void createTestReceipts() {
        for (User user : createdUsers) {
            for(int i = 0; i < new Random().nextInt(5); i++) {
                Receipt receipt = new Receipt();
                receipt.setUser(user);
                receipt.setShop(createdShops.get(new Random().nextInt(SHOPS_NUMBER - 1)));
                List<Product> products = new ArrayList<>();
                for(int j = 0; j < new Random().nextInt(3); j++) {
                    Product product = new Product();
                    product.setName(PRODUCT_NAME + String.valueOf(j));
                    product.setPrice(new Random().nextDouble() * 1000);
                    product.setQuantity(1 + new Random().nextInt(3));
                    products.add(product);
                }

                receipt.setProducts(products);
                receipt.setDateStored(RECEIPT_DATE_MIN + (int) (Math.random() * RECEIPT_DATE_RANGE));
                receiptRepository.save(receipt);
            }
        }
    }

    public void createTestMissingReceipts() {
        for(int i = 0; i < MISSING_RECEIPTS_NUMBER; i++) {
            MissingReceipt missingReceipt = new MissingReceipt();
            missingReceipt.setUser(createdUsers.get(new Random().nextInt(createdUsers.size() - 1)));
            missingReceipt.setDate(RECEIPT_DATE_MIN + (int) (Math.random() * RECEIPT_DATE_RANGE));
            missingReceipt.setShop(createdShops.get(new Random().nextInt(createdShops.size() - 1)));
            missingReceiptRepository.save(missingReceipt);
        }
    }

    public void createTestPromotions() {
        for(int i = 0; i < PROMOTIONS_NUMBER; i++) {
            Promotion promotion = new Promotion();

            promotion.setStartDate(RECEIPT_DATE_MIN + (int) (Math.random() * RECEIPT_DATE_RANGE));
            promotion.setEndDate(RECEIPT_DATE_MIN + RECEIPT_DATE_RANGE + (int) (Math.random() * RECEIPT_DATE_RANGE));
            promotion.setShop(createdUsers.get(new Random().nextInt(9)).getShop());
            promotion.setDescription(PROMOTION_DESCRIPTION);
            promotion.setName(PROMOTION_NAME);
            promotion.setTarget(PromotionTarget.values()[new Random().nextInt(3)]);
            promotion.setStatus(PromotionStatus.values()[new Random().nextInt(2)]);

            promotionRepository.save(promotion);
        }
    }

    public void createTestData() {
        createTestShops();
        createTestUsers();
        createTestReceipts();
        createTestMissingReceipts();
        createTestPromotions();
    }
}
