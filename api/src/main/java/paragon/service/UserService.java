package paragon.service;

import paragon.model.Token;
import paragon.model.db.User;
import paragon.model.dto.UserDTO;

/**
 * Created by 44072401 on 10/28/17.
 */
public interface UserService {

    Token login(UserDTO userDTO);

    User registerUser(UserDTO userDTO);

    void logout(UserDTO userDTO);

    User authenticate(String token);
}
