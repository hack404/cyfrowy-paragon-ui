package paragon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import paragon.exceptions.IncorrectPasswordException;
import paragon.exceptions.PasswordNotMatchingException;
import paragon.exceptions.UserAlreadyExistsException;
import paragon.exceptions.UserNotAuthenticatedException;
import paragon.model.Token;
import paragon.model.db.User;
import paragon.model.dto.UserDTO;
import paragon.repository.ShopRepository;
import paragon.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by 44072401 on 10/28/17.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ShopRepository shopRepository;

    @Override
    public Token login(UserDTO userDTO) {
        return Optional.ofNullable(userRepository.findFirstByEmail(userDTO.getEmail())).map(s -> {
            if(!s.getPassword().equals(userDTO.getPassword())) {
                throw new IncorrectPasswordException("Password Incorrect");
            } else {
                String token = userDTO.getEmail() + "|" + String.valueOf(UUID.randomUUID());
                s.setToken(token);
                userRepository.save(s);
                return new Token(token);
            }
        }).orElseThrow(() -> new EntityNotFoundException("User not found"));
    }

    @Override
    public User registerUser(UserDTO userDTO) {
        if (!userDTO.getPassword().equals(userDTO.getMatchingPassword())) {
            throw new PasswordNotMatchingException("Entered passwords do not match");
        }

        if( Optional.ofNullable(userRepository.findFirstByEmail(userDTO.getEmail())).isPresent()) {
            throw new UserAlreadyExistsException("User with this email address already exists");
        }

        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());
        user.setType(userDTO.getType());
        user.setNip(userDTO.getNip());
        if (userDTO.getShopId() != null) {
            Optional.ofNullable(shopRepository.findOne(userDTO.getShopId()))
                    .map(s -> {
                        user.setShop(s);
                        return s;
                    }).orElseThrow(() -> new EntityNotFoundException("Could not find shop with id: " + userDTO.getShopId()));
        }

        return userRepository.save(user);
    }

    @Override
    public void logout(UserDTO userDTO) {
        Optional.ofNullable(userRepository.findFirstByEmail(userDTO.getEmail())).map(s -> {
            s.setToken(null);
            userRepository.save(s);
            return s;
        }).orElseThrow(() -> new EntityNotFoundException("User not found"));

    }

    @Override
    public User authenticate(String token) {
        String email = token.split("\\|")[0];

        return Optional.ofNullable(userRepository.findFirstByEmail(email))
                .map(s -> {
                    if (s.getToken() == null || s.getToken().isEmpty() || !s.getToken().equals(token)) {
                        throw new UserNotAuthenticatedException("Wrong authentication token");
                    }
                    return s;
                }).orElseThrow(() -> new UserNotAuthenticatedException("User not found based on token"));
    }
}
