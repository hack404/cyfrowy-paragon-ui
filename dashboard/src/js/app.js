import {initMap} from './initMap.js';
import {chooseRegion} from "./chooseRegion";
import {chooseIndustry} from "./chooseIndustry";
import {chooseCompanies} from "./chooseCompanies";
import {chooseLackOfReceipts} from "./chooseLackOfReceipts";

window.initMap = initMap;


chooseRegion();
chooseIndustry();
chooseCompanies();
chooseLackOfReceipts();
