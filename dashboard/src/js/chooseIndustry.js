import {addData} from "./mapService";

export function chooseIndustry() {

    const industrySelect = document.getElementById('select-industry');

    industrySelect.addEventListener('change', function () {

        const xhr = new XMLHttpRequest();
        xhr.open('GET', `http://pawelkonor@panel13.mydevil.net:8085/stats/shops?sector=${industrySelect.value}`, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = function () {
            if (xhr.status === 200) {

                let response = JSON.parse(xhr.responseText);
                const markers = [];

                const map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 52.192820, lng: 19.261816},
                    zoom: 6
                });
                addData(response, markers, map);
            }
            else {
                console.log('Request failed.  Returned status of ' + xhr.status);
            }
        };
        xhr.send();
    });
}