import {addData} from "./mapService";

export function chooseLackOfReceipts () {

    const receiptsSelect = document.getElementById('select-receipts');
    const endpoint = 'http://pawelkonor@panel13.mydevil.net:8085/stats/shops/';
    const source = ['missing-receipt-user', 'missing-receipt-total'];

    receiptsSelect.addEventListener('change', function () {

        const xhr = new XMLHttpRequest();
        xhr.open('GET', `${endpoint}${source[receiptsSelect.value]}`, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = function() {
            if (xhr.status === 200) {

                let response = JSON.parse(xhr.responseText);
                const markers = [];

                const map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 52.192820, lng: 19.261816},
                    zoom: 6
                });

                addData(response, markers, map);
            }
            else {
                console.log('Request failed.  Returned status of ' + xhr.status);
            }
        };
        xhr.send();
    });
}