export function chooseRegion () {

    const regionSelect = document.getElementById('select-region');
    const regions = [
        {lat: 52.192820, lng: 19.261816},
        {lat: 51.115789, lng: 17.023441},
        {lat: 52.233696, lng: 20.999388},
        {lat: 51.757311, lng: 19.440759},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 50.062619, lng: 19.936074},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388},
        {lat: 52.233696, lng: 20.999388}
    ];

    regionSelect.addEventListener('change', function () {
        const map = new google.maps.Map(document.getElementById('map'), {
            center: regions[(parseInt(regionSelect.value, 10) + 2)],
            zoom: parseInt(regionSelect.value, 10) === 1 ? 6 : 8
        });
    });
}