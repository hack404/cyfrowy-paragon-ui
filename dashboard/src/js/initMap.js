export function initMap() {

    const mapStyle = [{
        'featureType': 'all',
        'elementType': 'all',
        'stylers': [{'visibility': 'on'}]
    }, {
        'featureType': 'landscape',
        'elementType': 'geometry',
        'stylers': [{'visibility': 'on'}, {'color': '#fcfcfc'}]
    }, {
        'featureType': 'water',
        'elementType': 'labels',
        'stylers': [{'visibility': 'off'}]
    }, {
        'featureType': 'water',
        'elementType': 'geometry',
        'stylers': [{'visibility': 'on'}, {'hue': '#5f94ff'}, {'lightness': 1}]
    }];

    const map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 52.192820, lng: 19.261816},
        zoom: 6,
        styles: mapStyle
    });


    // addData();
}

// var markers = [];
//
// var citymap = {
//     warsaw: {
//         center: {lat: 52.233696, lng: 20.999388},
//         population: 2714856,
//         color: 'blue'
//     },
//     cracov: {
//         center: {lat: 50.059252, lng: 19.941911},
//         population: 8405837,
//         color: 'red'
//     }
// };
//
// function initMap() {
//     map = new google.maps.Map(document.getElementById('map'), {
//         center: {lat: 52.192820, lng: 19.261816},
//         zoom: 6,
//         styles: mapStyle
//     });

// addData();
// }

// function moveToPoint(map) {
//     map.setCenter = {lat: 52.233696, lng: 20.999388};
// }
//
//
// function addData() {
//     for (var city in citymap) {
//         // Add the circle for this city to the map.
//         var marker = new google.maps.Circle({
//             strokeColor: citymap[city].color,
//             strokeOpacity: 0.8,
//             strokeWeight: 2,
//             fillColor: citymap[city].color,
//             fillOpacity: 0.35,
//             map: map,
//             center: citymap[city].center,
//             radius: Math.sqrt(citymap[city].population) * 100
//         });
//         markers.push(marker);
//
//     }
//     animateCircle();
//
// }
//
// function animateCircle() {
//     var direction = 1;
//     intID = setInterval(function () {
//         var radius = markers[0].getRadius();
//         ;
//         if ((radius > Math.sqrt(citymap.warsaw.population) * 100) || (radius < 0)) {
//             direction *= -1;
//         }
//         markers[0].setRadius(radius + direction * 1080);
//     }, 20);
// }
//
//
// function setMapOnAll(map) {
//     for (var i = 0; i < markers.length; i++) {
//         markers[i].setMap(map);
//     }
// }
//
//
// function clearMarkers() {
//     setMapOnAll(null);
// }
//
// function showMarkers() {
//     setMapOnAll(map);
// }
//
//