export function addData(response, markers, map) {
    for (var shop in response) {
        var marker = new google.maps.Circle({
            strokeColor: response[shop].color,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: response[shop].color,
            fillOpacity: 0.35,
            map: map,
            center: {lat: response[shop].latitude, lng: response[shop].longitude},
            radius: 20000
        });
        markers.push(marker);
    }
}

export function animateCircle() {
    var direction = 1;
    intID = setInterval(function () {
        var radius = markers[0].getRadius();
        if ((radius > 100000 || (radius < 0))) {
            direction *= -1;
        }
        markers[0].setRadius(radius + direction * 1080);
    }, 20);
}

export function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

export function clearMarkers() {
    setMapOnAll(null);
}

export function showMarkers() {
    setMapOnAll(map);
}