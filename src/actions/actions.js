export const ACTION_LAST_STARTED = 'ACTION_LAST_STARTED';
export const ACTION_LAST_COMPLETED = 'ACTION_LAST_COMPLETED';

export function startAction() {
    return {
        type: ACTION_LAST_STARTED
    };
}

export function completeAction(message = '') {
    return {
        type: ACTION_LAST_COMPLETED,
        message
    };
}
