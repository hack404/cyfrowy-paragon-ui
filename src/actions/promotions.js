import Config from '../Config';

export const PROMOTIONS_LOADED = 'PROMOTIONS_LOADED';

export function getPromotions() {
    return (dispatch, getState) => {
        const { user } = getState();

        fetch(Config.api.url + '/promotions/all', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authentication': user.token
            }
        }).then((res) => {
            return res.json();
        }).then((promotions) => {
            dispatch(promotionsLoaded(promotions));
        }).catch((err) => {
            console.error(err);
        });
    };
}

function promotionsLoaded(promotions) {
    return {
        type: PROMOTIONS_LOADED,
        promotions
    };
}
