import Expo from 'expo';
import Config from '../Config';
import { startAction, completeAction } from './actions';

export const RECEIPT_ADDED = 'RECEIPT_ADDED';
export const RECEIPTS_LOADED = 'RECEIPTS_LOADED';
export const RECEIPTS_MISSING_REPORTED = 'RECEIPTS_MISSING_REPORTED';

export function addReceipt(receipt) {
    return (dispatch, getState) => {
        const { user } = getState();

        fetch(Config.api.url + '/receipts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authentication': user.token
            },
            body: JSON.stringify(receipt)
        }).then((res) => {
            return res.json();
        }).then((res) => {
            delete receipt.products;
            receipt.date = new Date(receipt.date).getTime();

            dispatch(receiptAdded(receipt));
        }).catch((err) => {
            console.error(err);
        });
    };
}

export function getReceipts() {
    return (dispatch, getState) => {
        const { user } = getState();

        fetch(Config.api.url + '/receipts/all', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authentication': user.token
            }
        }).then((res) => {
            return res.json();
        }).then((receipts) => {
            dispatch(receiptsLoaded(receipts));
        }).catch((err) => {
            console.error(err);
        });
    };
}

export function reportMissingReceipt(opts) {
    return (dispatch, getState) => {
        const { user } = getState();

        opts.shopName = 'Biedrzonka';
        opts.latitude = 19.9877032;
        opts.longitude = 50.0785781;
        delete opts.city;

        dispatch(startAction());

        setTimeout(() => {
            dispatch(completeAction());
        }, 500);

        setTimeout(() => {
            Expo.Notifications.presentLocalNotificationAsync({
                title: 'Skończyłeś zakupy?',
                vibrate: true
            });
            // PushNotification.localNotification({
            //      message: 'Skończyłeś zakupy?',
            //      actions: '[ "Dodaj paragon", "Zgłoś brak" ]'
            // });
        }, 2000);
        // fetch(Config.api.url + '/receipts/missing', {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         'Authentication': user.token
        //     },
        //     body: JSON.stringify(opts)
        // }).then((res) => {
        //     return res.json();
        // }).then((res) => {
        //     dispatch(missingReceiptReported());
        //     dispatch(completeAction());
        // }).catch((err) => {
        //     console.error(err);
        // });
    }
}

function receiptAdded(receipt) {
    return {
        type: RECEIPT_ADDED,
        receipt
    };
}

function receiptsLoaded(receipts) {
    return {
        type: RECEIPTS_LOADED,
        receipts
    };
}

function missingReceiptReported() {
    return {
        type: RECEIPTS_MISSING_REPORTED
    };
}
