import Config from '../Config';

export const USER_LOGGED_IN = 'USER_LOGGED_IN';

export function loginUser(email, password) {
    return (dispatch) => {
        email = 'jan.kowalski5@gmail.com';
        password = '12345';

        console.log('requesting userdata');
        fetch(Config.api.url + '/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,
                password
            })
        }).then((res) => {
            console.log('res', res);
            return res.json();
        }).then((res) => {
            console.log('userdata', res);
            dispatch(userLoggedIn({ token: res.token, email, password }));
        }).catch((err) => {
            console.error(err);
        });
    };
}

function userLoggedIn(user) {
    return {
        type: USER_LOGGED_IN,
        user
    };
}