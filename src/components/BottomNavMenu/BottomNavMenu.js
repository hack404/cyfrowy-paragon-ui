import React, { Component} from 'react';
import Expo from 'expo';

import { Modal, View } from 'react-native';
import {
    Button,
    FooterTab,
    Icon,
    Text
} from 'native-base';

export default class BottomNavMenu extends Component {
    constructor() {
        super();

        this.setAddReceiptModalVisible = this._setAddReceiptModalVisible.bind(this);
        this.state = {
            popup: false
        };

        if(global.notificationSubscriber) global.notificationSubscriber.remove();

        global.notificationSubscriber = Expo.Notifications.addListener((notification) => {
            if(notification.origin !== 'selected') return;

            if(this.state.popup === false) this.setAddReceiptModalVisible(true)();
        });
    }

    _setAddReceiptModalVisible(v) {
        return () => {
            this.setState({ ...this.state, popup: v });
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        const goTo = (view) => {
            return () => { navigate(view); };
        };

        return (
            <FooterTab>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.popup}
                    onRequestClose={() => {}}
                >
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Button onPress={() => { goTo('ScanReceipt')(); this.setAddReceiptModalVisible(false)(); }}>
                            <Text style={{fontSize: 20}}>Zeskanuj</Text>
                        </Button>
                        <Button onPress={() => { goTo('MissingReceipt')(); this.setAddReceiptModalVisible(false)(); }}>
                            <Text style={{fontSize: 20}}>Nie dostałem(am) paragonu</Text>
                        </Button>
                        <Button onPress={this.setAddReceiptModalVisible(false)}>
                            <Text style={{fontSize: 20}}>Powrót</Text>
                        </Button>
                    </View>
                </Modal>
                <Button
                    onPress={this.setAddReceiptModalVisible(true)}
                >
                    <Icon name="md-add" />
                </Button>
                <Button
                    onPress={goTo('Receipts')}
                >
                    <Icon name="funnel" />
                </Button>
                <Button
                    onPress={goTo('Promotions')}
                >
                    <Icon name="people" />
                </Button>
                <Button
                    onPress={goTo('Lottery')}
                >
                    <Icon name="barcode" />
                </Button>
                <Button
                    onPress={goTo('AddPromotion')}
                >
                    <Icon name="trending-down" />
                </Button>
            </FooterTab>
        );
    }
}
