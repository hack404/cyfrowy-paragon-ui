import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Image} from 'react-native';
import {StyleSheet} from 'react-native';
import {
    Body,
    Button,
    Card,
    CardItem,
    Icon,
    Input,
    Item,
    Text
} from 'native-base';

import {loginUser} from '../../actions/users';

export class LoginForm extends Component {
    static get defaultProps() {
        return {
            onLoginSuccess: () => {
            },
            onLoginError: () => {
            },
            onRegisterOpen: () => {
            },
            showRegisterButton: true
        };
    }

    constructor() {
        super();

        this.state = {
            email: '',
            password: ''
        };

        this.login = this._login.bind(this);
        this.updateField = this._updateField.bind(this);
    }

    componentWillReceiveProps(props) {
        if (props.user.token) {
            this.props.onLoginSuccess(props.user.email, props.user.token);
        }
    }

    _login() {
        this.props.loginUser(this.state.email, this.state.password);
    }

    _updateField(name) {
        return (val) => {
            this.setState({
                ...this.state,
                [name]: val
            });
        };
    }

    render() {
        const {navigate} = this.props.navigation;

        return (
            <Card style={{ padding: 5 }}>
                <CardItem header style={styles.headerContainer}>
                    <Image
                        style={{width: 180, height: 180 }}
                        source={require('../../../resources/images/logo.png')}
                        alignContent="center"
                    />
                </CardItem>
                <CardItem style={{display: 'flex', justifyContent: 'center'}}>
                    <Text style={styles.headerText}>Sign In</Text>
                </CardItem>
                <CardItem>
                    <Body>
                    <Item>
                        <Input placeholder="E-mail" onChangeText={this.updateField('email')}/>
                    </Item>
                    <Item>
                        <Input placeholder="Password" onChangeText={this.updateField('password')}/>
                    </Item>
                    </Body>
                </CardItem>
                <CardItem footer>
                    <Button style={styles.actionButton} onPress={this.login}>
                        <Text>Login</Text>
                    </Button>
                    {this.props.showRegisterButton ? (
                        <Button
                            style={styles.actionButton}
                            onPress={() =>
                            navigate('Register')
                        }
                        >
                            <Text>Register</Text>
                        </Button>
                    ) : null}
                    <Button color="#fff" onPress={() => navigate('SelfLearn')} style={styles.actionButton}>
                        <Icon name="information-circle" style={{color: 'white'}} />
                    </Button>
                </CardItem>
            </Card>
        );
    }
}

export default connect((state) => ({
    user: state.user
}), (dispatch) => ({
    loginUser: (email, password) => {
        dispatch(loginUser(email, password));
    }
}))(LoginForm);

const styles = StyleSheet.create({
    actionButton: {
        marginLeft: 3,
        marginRight: 3,
        flex: 1,
        justifyContent: 'center'
    },
    headerContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 30,
        fontWeight: 'bold'
    }
});
