import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Camera, Permissions } from 'expo';
import getReceipt from '../../lib/receipt-parsers';

import {
    Body,
    Button,
    Container,
    Content,
    Footer,
    FooterTab,
    Text
} from 'native-base';

const ocrResultStub = [
    'PIT STOP Sp. z o.o',
    '32-043 Skała, ul. Krakowska 82',
    'Carrefour EXPRESS Kraków',
    '31-079 Kraków',
    'ul. Dietla 40',
    'NIP 677-2222-56-47',
    '2017-10-14                 nr wydr.036245',
    'PARAGON FISKALNY',
    'D_MLEKO LACIATE 3,2            1*3,45= 3,45 D',
    'D_MC KULECZKI 250G             1*2,79= 2,79 D',
    'D_JOGOBELLA 400G ST            1*2,79= 2,79 D',
    'D_MASLO EKSTRA LACI            1*5,99= 5,99 D',
    'D_BULKA MALA 50G RA            3*0,59= 1,77 D',
    'D_BULKA DUZA 70G RA            2*0,79= 1,58 D',
    'Sprzed. opod. PTU D            18,37',
    'Kwota D 05,00%                 0,87',
    'Podatek PTU                    0,87',
    'SUMA PLN                       18,37',
    '000034 #002 002                16:01',
    'C9TUJ-4QS6M-T4XSU-PVFFR-PWDI5',
    'BGI 15490012',
    '0 0 6 4 0 0 0 2 2 5 6 9 9 1 7 1 0 1 4 1',
    'płatność           Gotówka     18,37',
    'Kasjer             002',
    'Nr transakcji      25699',
    'Reklamacje z paragonem'
];

export default class ReceiptScanner extends Component {
    static get defaultProps() {
        return {
            onScanComplete: () => {}
        };
    }

    constructor() {
        super();

        this.takePicture = this._takePicture.bind(this);

        this.state = {
            cameraHasPermission: null
        };
    }

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);

        this.setState({ cameraHasPermission: status === 'granted' });
    }

    async _takePicture() {
        if(!this.camera) return;

        let photo = await this.camera.takePictureAsync();
        let ocrResult = await this.ocrReceipt();
        const receipt = getReceipt(ocrResult);

        this.props.onScanComplete(receipt);
    }

    async ocrReceipt() {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(ocrResultStub);
            }, 500);
        });
    }

    render() {
        const { cameraHasPermission } = this.state;

        if(cameraHasPermission === null) return <Text>loading permissions</Text>;
        if(cameraHasPermission === false) return <Text>no permission for camera</Text>;

        return (
            <Camera ref={(cam) => { this.camera = cam; }} style={styles.camera}>
                <Container>
                    <Content>
                    </Content>
                    <Footer>
                        <FooterTab>
                            <Button onPress={this.takePicture}>
                                <Text>Scan</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Camera>
        );
    }
}

const styles = StyleSheet.create({
    camera: {
        flex: 1
    }
});
