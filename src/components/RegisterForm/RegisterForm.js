import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {
    View,
    Navigator,
    Body,
    Button,
    Container,
    Content,
    Footer,
    FooterTab,
    Header,
    Icon,
    Right,
    Text,
    Title,
    Left,
    Card,
    CardItem,
    Item,
    Picker,
    Input,
} from 'native-base';

export default class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: undefined,
        };
    }

    static get defaultProps() {
        return {
            onRegisterSuccess: () => {
            },
            onRegisterError: () => {
            },
            showRegisterButton: true
        };
    }

    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <Container>
                <Header style={{ padding: 0, height:100 }}>
                    <Left>
                        <Button style={{ paddingTop: 15}} transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title>Register Page</Title>
                    </Body>
                    <Right />
                </Header>

                <Card style={{ padding: 5 }}>
                    <CardItem header style={styles.headerContainer}>
                        <Text style={styles.headerText}>Register</Text>
                    </CardItem>
                    <CardItem>
                        <Body>
                        <Item>
                            <Input placeholder="E-mail"/>
                        </Item>
                        <Item>
                            <Input placeholder="Password"/>
                        </Item>
                        <Item>
                            <Picker
                                mode="dropdown"
                                headerStyle={{ backgroundColor: "#b95dd3" }}
                                headerBackButtonTextStyle={{ color: "#fff" }}
                                headerTitleStyle={{ color: "#fff" }}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                <Item label="Wallet" value="key0" />
                                <Item label="ATM Card" value="key1" />
                                <Item label="Debit Card" value="key2" />
                                <Item label="Credit Card" value="key3" />
                                <Item label="Net Banking" value="key4" />
                            </Picker>
                        </Item>
                        </Body>
                    </CardItem>
                    <CardItem footer>
                        <Button
                            style={styles.actionButton}
                            onPress={() =>
                                navigate('Home')
                            }
                        >
                            <Text>Add account</Text>
                        </Button>
                    </CardItem>
                </Card>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    actionButton: {
        marginLeft: 3,
        marginRight: 3
    },
    headerContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 30,
        fontWeight: 'bold'
    }
});
