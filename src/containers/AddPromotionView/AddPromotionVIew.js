import React, { Component } from 'react';
import Expo from 'expo';

import { StyleSheet, Platform } from 'react-native';
import {
    View,
    Navigator,
    Body,
    Button,
    Container,
    Content,
    Footer,
    FooterTab,
    Header,
    Icon,
    Right,
    Text,
    Title,
    CardItem,
    Item,
    Input,
} from 'native-base';
import BottomNavMenu from '../../components/BottomNavMenu';

export default class AddPromotionVIew extends Component {
    render() {
        const { navigate } = this.props.navigation;
        const goTo = (view) => {
            return () => {
                navigate(view);
            };
        };
        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Title>Company Promotion Creator</Title>
                    <Right style={styles.topBarActionContainer}>
                        <Button onPress={goTo('Setup')} transparent>
                            <Icon name="cog"/>
                        </Button>
                    </Right>
                </Header>
                <Content style={styles.contentContainer}>
                    <CardItem>
                        <Body>
                            <Item>
                                <Text>List of active promotions</Text>
                            </Item>
                            <Item>
                                <Input placeholder="name"/>
                            </Item>
                            <Item>
                                <Input placeholder="Description"/>
                            </Item>
                            <Item>
                                <Input placeholder="Duration"/>
                            </Item>
                            <Item>
                                <Input placeholder="Target"/>
                            </Item>
                        </Body>
                    </CardItem>
                    <CardItem footer>
                        <Button>
                            <Icon name="md-add"/>
                            <Text>Add promotion</Text>
                        </Button>
                    </CardItem>
                </Content>
                <Footer>
                    <BottomNavMenu navigation={this.props.navigation} />
                </Footer>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        display: 'flex',
        alignItems: 'center',
        position: 'relative'
    },
    actionButton: {
        marginLeft: 3,
        marginRight: 3
    },
    headerContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 30,
        fontWeight: 'bold'
    }
});