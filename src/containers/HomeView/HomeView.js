import React, {Component} from 'react';
import {Platform, StyleSheet, Image, Dimensions} from 'react-native';
import Expo from 'expo';
import resolveAssetSource from 'resolveAssetSource';

import {
    Body,
    Button,
    Container,
    Content,
    Footer,
    FooterTab,
    Header,
    Icon,
    Left,
    Right,
    Text,
    Title
} from 'native-base';
import BottomNavMenu from '../../components/BottomNavMenu';

export default class HomeView extends Component {
    constructor() {
        super();

        this.state = {
            image: {
                w: 0,
                h: 0
            }
        };
    }

    componentWillMount() {
        this.image = require('../../../resources/images/poland-flag-map.png');
    }

    componentDidMount() {
        const {width, height} = resolveAssetSource(this.image);

        const screenW = Dimensions.get('window').width - 40;
        const ratio = width / screenW;
        const scale = height / ratio;

        this.setState({image: {w: screenW, h: scale}});
    }

    render() {
        const { navigate } = this.props.navigation;
        const goTo = (view) => {
            return () => {
                navigate(view);
            };
        };

        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Left>
                        <Image style={styles.logo} source={require('../../../resources/images/logo.png')} />
                    </Left>
                    <Body>
                        <Title>Paragon Cyfrowy</Title>
                    </Body>
                    <Right>
                        <Button onPress={goTo('Setup')} transparent>
                            <Icon name="cog"/>
                        </Button>
                    </Right>
                </Header>
                <Content style={styles.contentContainer}>
                    <Image
                        source={this.image}
                        resizeMode="contain"
                        style={{ width: this.state.image.w, height: this.state.image.h}}
                    />
                    <Text
                        style={{textAlign : 'center', height: 100, marginTop: 20}}
                    >When you collect your receipts you pay for the taxes paid by the seller, so you care about Poland and the future of your children.</Text>
                </Content>
                <Footer>
                    <BottomNavMenu navigation={this.props.navigation}/>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        position: 'relative'
    },
    logo: {
        width: 50,
        height: 50
    },
    contentContainer: {
        padding: 20
    }
});