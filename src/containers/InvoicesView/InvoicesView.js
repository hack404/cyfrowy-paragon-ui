import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';

import {
    Button,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Image,
    Right,
    Text,
    Title
} from 'native-base';
import BottomNavMenu from '../../components/BottomNavMenu';

export default class InvoicesView extends Component {
    static get defaultProps() {
        return {
            title: 'Paragony'
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        const goTo = (view) => {
            return () => { navigate(view); };
        };

        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Title>Receipts</Title>
                    <Right style={styles.topBarActionContainer}>
                        <Button onPress={goTo('Setup')} transparent>
                            <Icon name="cog" />
                        </Button>
                    </Right>
                </Header>
                <Content style={styles.contentContainer}>
                    <Text>Receipts</Text>
                </Content>
                <Footer>
                    <BottomNavMenu navigation={this.props.navigation} />
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        display: 'flex',
        alignItems: 'center',
        position: 'relative'
    },
    contentContainer: {
        padding: 20
    },
});