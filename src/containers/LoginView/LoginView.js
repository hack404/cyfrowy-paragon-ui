import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoginForm from '../../components/LoginForm';

import { getReceipts } from '../../actions/receipts';
import { getPromotions } from '../../actions/promotions';

export class LoginView extends Component {
    constructor() {
        super();

        this.loginSuccess = this._loginSuccess.bind(this);
    }

    _loginSuccess(email, token) {
        console.log('logging');
        this.props.getReceipts();
        console.log('got receipts');
        this.props.getPromotions();
        console.log('got promotions');
        this.props.navigation.navigate('Home');
    }

    render() {
        return (
            <LoginForm
                onLoginSuccess={this.loginSuccess}
                navigation={this.props.navigation}
                onRegisterOpen={this.openRegister}
            />
        );
    }
}

export default connect((state) => ({
}), (dispatch) => ({
    getReceipts: () => { dispatch(getReceipts()); },
    getPromotions: () => { dispatch(getPromotions()); }
}))(LoginView);