 import React, { Component } from 'react';
import { connect } from 'react-redux';

import { StyleSheet, Platform } from 'react-native';
import {
    Body,
    Button,
    Card,
    CardItem,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Image,
    Input,
    Item,
    Right,
    Text,
    Title
} from 'native-base';
import BottomNavMenu from '../../components/BottomNavMenu';

import { reportMissingReceipt } from '../../actions/receipts';

export class MissingReceiptView extends Component {
    constructor() {
        super();

        this.state = {
            form: {
                shopName: '',
                city: ''
            }
        };

        this.updateField = this._updateField.bind(this);
        this.save = this._save.bind(this);
    }

    componentWillReceiveProps(props) {
        if(!this.props.lastAction) return;

        if(this.props.lastAction.pending && !props.lastAction.pending) {
            this.props.navigation.navigate('Home');
        }
    }

    _updateField(name) {
        return (val) => {
            this.setState({
                ...this.state,
                form: {
                    ...this.state.form,
                    [name]: val
                }
            });
        };
    }

    _save() {
        this.props.reportMissingReceipt(this.state.form);
    }

    render() {
        const { navigate } = this.props.navigation;
        const goTo = (view) => {
            return () => { navigate(view); };
        };

        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Title>Report no receipt</Title>
                    <Right style={styles.topBarActionContainer}>
                        <Button onPress={goTo('Setup')} transparent>
                            <Icon name="cog" />
                        </Button>
                    </Right>
                </Header>
                <Content style={styles.contentContainer}>
                    <Card>
                        <CardItem>
                            <Body>
                            <Item>
                                <Input placeholder="Shop" onChangeText={this.updateField('shopName')} />
                            </Item>
                            <Item>
                                <Input placeholder="City" onChangeText={this.updateField('city')} />
                            </Item>
                        </Body>
                        </CardItem>
                        <CardItem footer>
                            <Button onPress={this.save}>
                                <Text>Save</Text>
                            </Button>
                        </CardItem>
                    </Card>
                </Content>
                <Footer>
                    <BottomNavMenu navigation={this.props.navigation} />
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        display: 'flex',
        alignItems: 'center',
        position: 'relative'
    },
    contentContainer: {
        padding: 20
    },
});

export default connect((state) => ({
    lastAction: state.ui.lastAction
}), (dispatch) => ({
    reportMissingReceipt: (opts) => { dispatch(reportMissingReceipt(opts)); }
}))(MissingReceiptView);
