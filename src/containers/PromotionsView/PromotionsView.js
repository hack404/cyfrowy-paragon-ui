import React, { Component } from 'react';
import { connect } from 'react-redux';
import Expo from 'expo';
import { StyleSheet, Platform } from 'react-native';

import {
    Button,
    Card,
    CardItem,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Image,
    Right,
    Text,
    Title
} from 'native-base';
import BottomNavMenu from '../../components/BottomNavMenu';

class PromotionsView extends Component {
    static get defaultProps() {
        return {
            promotions: []
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        const goTo = (view) => {
            return () => { navigate(view); };
        };

        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Title>Promotions List</Title>
                    <Right style={styles.topBarActionContainer}>
                        <Button onPress={goTo('Setup')} transparent>
                            <Icon name="cog" />
                        </Button>
                    </Right>
                </Header>
                <Content style={styles.contentContainer}>
                    {this.props.promotions.map((r, i) => {
                        return (
                            <Card key={i} onTouchEnd={goTo('Receipt', { receipt: r })} style={{ marginTop: i === 0 ? 20 : 0 }}>
                                <CardItem style={{justifyContent: 'space-between'}}>
                                    <Text style={styles.itemName}>{r.name}</Text>
                                    <Text style={styles.itemValue}>{r.description}</Text>
                                </CardItem>
                            </Card>
                        );
                    })}
                </Content>
                <Footer>
                    <BottomNavMenu navigation={this.props.navigation} />
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        display: 'flex',
        alignItems: 'center',
        position: 'relative'
    },
    contentContainer: {
        padding: 20
    },
    itemName: {
    },
    itemValue: {
        fontSize: 20,
        fontWeight: 'bold',
    }
});

export default connect((state) => ({
    promotions: state.promotions
}))(PromotionsView);