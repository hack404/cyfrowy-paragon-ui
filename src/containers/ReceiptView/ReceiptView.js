import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Platform } from 'react-native';
import Expo from 'expo';

import {
    Body,
    Button,
    Card,
    CardItem,
    Container,
    Content,
    Header,
    Text,
    Title
} from 'native-base';

import { addReceipt } from '../../actions/receipts';

const renderDate = (d) => {
    const prefixWithZero = (v) => ('0000' + v).substr(-2, 2);

    const year = d.getFullYear();
    const month = prefixWithZero(d.getMonth());
    const day = prefixWithZero(d.getDate());
    const hour = prefixWithZero(d.getHours());
    const minutes = prefixWithZero(d.getMinutes());

    return `${year}-${month}-${day} ${hour}:${minutes}`;
}

export class ReceiptView extends Component {
    constructor() {
        super();

        this.state = {
            receipt: null
        };

        this.save = this._save.bind(this);
    }

    componentWillMount() {
        const isReceiptCtrl = typeof this.props.navigation.state.params.receipt.get === 'function';
        let r = this.props.navigation.state.params.receipt;
        let showActions = false;

        if(isReceiptCtrl) {
            r = {
                ...r.get(),
                totalPrice: r.getProducts()
                    .map((p) => parseFloat(p.price) * parseInt(p.quantity), 10)
                    .reduce((p1, p2) => p1 + p2, 0),
                
            }

            showActions = true;
        }

        this.setState({
            ...this.state,
            receipt: r,
            showActions
        });
    }

    componentWillReceiveProps(props) {
        if(props.receipts.length > this.props.receipts.length) {
            this.receiptAdded();
        }
    }

    receiptAdded() {
        this.props.navigation.navigate('Receipts');
    }

    _save() {
        this.props.addReceipt(this.state.receipt);
    }

    render() {
        if(!this.state.receipt) return null;

        const { navigate } = this.props.navigation;
        const goTo = (view) => () => { navigate(view) };
        const r = this.state.receipt;

        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Title>Recipt</Title>
                </Header>
                <Content>
                    <Card>
                        <CardItem>
                            <Body style={styles.container}>
                                <Text style={styles.header}>Podsumowanie</Text>
                                <Text>Nazwa sklepu: {r.shopName}</Text>
                                <Text>Data: {renderDate(new Date(r.date))}</Text>
                                <Text>Wartość: {r.totalPrice.toFixed(2)}zł</Text>
                            </Body>
                        </CardItem>
                        {this.state.showActions ? (
                            <CardItem>
                                <Text>Czy dane są poprawne?</Text>
                            </CardItem>
                        ) : null}
                        {this.state.showActions ? (
                            <CardItem footer>
                                <Button style={styles.actionButton} onPress={this.save}>
                                    <Text>Tak</Text>
                                </Button>
                                <Button style={styles.actionButton} onPress={goTo('Home')}>
                                    <Text>Nie</Text>
                                </Button>
                            </CardItem>
                        ) : null}
                    </Card>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        display: 'flex',
        alignItems: 'center',
        position: 'relative'
    },
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    header: {
        fontSize: 22,
        fontWeight: 'bold',
        flex: 1
    },
    listRow: {
        flex: 1
    },
    listRowItem: {
        flex: 1
    },
    actionButton: {
        marginLeft: 3,
        marginRight: 3
    }
});

export default connect((state) => ({
    receipts: state.receipts
}), (dispatch) => ({
    addReceipt: (receipt) => { dispatch(addReceipt(receipt)); }
}))(ReceiptView);