import React, { Component } from 'react';
import {
    View,
    Navigator,
    Body,
    Button,
    Container,
    Content,
    Footer,
    FooterTab,
    Header,
    Icon,
    Right,
    Text,
    Title
} from 'native-base';

import RegisterForm from '../../components/RegisterForm';


export default class RegisterView extends Component {
    static get defaultProps() {
        return {
            title: 'Register'
        };
    }

    render() {
        return (
            <RegisterForm
                navigation={this.props.navigation}
            />
        )
    }
}