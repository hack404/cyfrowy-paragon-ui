import React, { Component } from 'react';

import ReceiptScanner from '../../components/ReceiptScanner';

export default class ScanReceiptView extends Component {
    constructor() {
        super();

        this.scanCompleted = this._scanCompleted.bind(this);
    }

    _scanCompleted(receipt) {
        this.props.navigation.navigate('Receipt', { receipt });
    }

    render() {
        return (
            <ReceiptScanner onScanComplete={this.scanCompleted} />
        );
    }
}
