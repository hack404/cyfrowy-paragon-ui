import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import Expo from 'expo';

import {
    Button,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Image,
    Right,
    Text,
    Title,
    Left,
    Body,
} from 'native-base';
import BottomNavMenu from '../../components/BottomNavMenu';

export default class SelfLearnView extends Component {
    render() {
        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Tutorial</Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={styles.contentContainer}>
                    <Text>
                    Consumer Tutorial
                    Uploading every receipt you receive will gain you:
                     - history of your expenses, with stats and analysis - all in one place,
                     - proof of purchase required for complaint processing,
                     - special promotion and loyalty programs from your favourite stores,
                     - additional lotteries and contests,
                    </Text>
                </Content>
                <Content style={styles.contentContainer}>
                    <Text>
                    Seller Tutorial
                    Application allows you to:
                     - use free and direct communication channel with your customers,
                     - increase your sales by creating targeted promotions,
                    - build up a free data base of your actual customers and their shopping habits,</Text>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        display: 'flex',
        alignItems: 'center',
        position: 'relative'
    },
    contentContainer: {
        padding: 20
    },
});