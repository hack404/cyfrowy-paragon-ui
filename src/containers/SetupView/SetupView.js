import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';

import {
    Button,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Image,
    Right,
    Text,
    Title
} from 'native-base';
import BottomNavMenu from '../../components/BottomNavMenu';

export default class SetupView extends Component {
    static get defaultProps() {
        return {
            title: 'Paragony'
        };
    }

    render() {
        return (
            <Container>
                <Header style={styles.statusBar}>
                    <Title>Settings</Title>
                </Header>
                <Content style={styles.contentContainer}>
                    <Text>Settings</Text>
                </Content>
                <Footer>
                    <BottomNavMenu navigation={this.props.navigation} />
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    statusBar: {
        marginTop: Platform.OS === 'android' ? Expo.Constants.statusBarHeight : 0,
        display: 'flex',
        alignItems: 'center',
        position: 'relative'
    },
    contentContainer: {
        padding: 20
    },
});