import standardParser from './parsers/standard';

const parsers = [
    standardParser
];

export default function getReceipt(raw) {
    const matched = parsers
        .map((p) => new p(raw))
        .filter((p) => p.isMatched());

    if(matched) return matched[0];
}