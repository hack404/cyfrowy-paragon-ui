export default class Receipt {
    constructor(raw) {
        this.raw = raw;
    }

    isMatched() {
        const hasNIP = this.raw.some((i) => /NIP [0-9]{3}-[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(i));

        return ([
            hasNIP
        ]).every((v) => v);
    }

    getShopData() {
        return {
            name: this.raw[2]
        };
    }

    getDate() {
        const nipIndex = this.raw.findIndex((i) => /NIP [0-9]{3}-[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(i));
        const dateIndex = nipIndex + 1;
        const date = this.raw[dateIndex].match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}/);
        const time = this.getTime();

        if(date) {
            const d = new Date();
            d.setFullYear(date[0].split('-')[0]);
            d.setMonth(date[0].split('-')[1]);
            d.setDate(date[0].split('-')[2]);
            d.setHours(time.split(':')[0]);
            d.setMinutes(time.split(':')[1]);
            
            return d.getTime();
        }

        return null;
    }

    getTotal() {
        const totalIndex = this.raw.findIndex((i) => /^SUMA PLN/.test(i));
        const total = this.raw[totalIndex].match(/[0-9]+,[0-9]{2}/);

        if(total) return total[0];

        return null;
    }

    getTime() {
        const totalIndex = this.raw.findIndex((i) => /^SUMA PLN/.test(i));
        const timeIndex = totalIndex + 1;
        const time = this.raw[timeIndex].match(/[0-9]{2}:[0-9]{2}$/);

        if(time) return time[0];

        return null;
    }

    getProducts() {
        const startIndex = this.raw.findIndex((i) => /PARAGON FISKALNY/.test(i)) + 1;
        const endIndex = this.raw.findIndex((i) => /Sprzed. opod./.test(i));

        return this.raw
            .slice(startIndex, endIndex)
            .map((r) => r.split(/\s{2,}/g))
            .map((r) => {
                const name = r[0];
                const price = r[1].match(/\*[0-9]+,[0-9]{2}/)[0].replace('*', '').replace(',', '.');
                const quantity = r[1].match(/^[0-9]+/)[0];

                return {
                    name,
                    price,
                    quantity
                };
            });
    }

    get() {
        return {
            shopAddress: `${this.raw[4]} ${this.raw[3]}`,
            shopName: this.getShopData().name,
            date: this.getDate(),
            time: this.getTime(),
            products: this.getProducts()
        }
    }
}
