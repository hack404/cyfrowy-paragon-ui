import { combineReducers } from 'redux';
import * as user from './users';
import * as receipts from './receipts';
import * as ui from './ui';
import * as promotions from './promotions';

export default combineReducers({
    ...user,
    ...receipts,
    ...ui,
    ...promotions
});