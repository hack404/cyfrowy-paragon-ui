import { PROMOTIONS_LOADED } from '../actions/promotions';

const initialState = [];

export function promotions(state = initialState, action) {
    switch (action.type) {
        // case PROMOTIONS_ADDED:
        //     return [ ...state, action.promotions ];
        case PROMOTIONS_LOADED:
            return [ ...action.promotions ];
        default:
            return state;
    }
}