import { RECEIPT_ADDED, RECEIPTS_LOADED } from '../actions/receipts';

const initialState = [];

export function receipts(state = initialState, action) {
    switch (action.type) {
        case RECEIPT_ADDED:
            return [ ...state, action.receipt ];
        case RECEIPTS_LOADED:
            return [ ...action.receipts ];
        default:
            return state;
    }
}