import { ACTION_LAST_STARTED, ACTION_LAST_COMPLETED } from '../actions/actions';

const initialState = {};

export function ui(state = initialState, action) {
    switch (action.type) {
        case ACTION_LAST_STARTED:
            return { ...state, lastAction: { pending: true, message: '' } };
        case ACTION_LAST_COMPLETED:
            return { ...state, lastAction: { pending: false, message: action.message } }
        default:
            return state;
    }
}