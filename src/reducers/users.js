import { USER_LOGGED_IN } from '../actions/users';

const initialState = {};

export function user(state = initialState, action) {
    switch (action.type) {
        case USER_LOGGED_IN:
            return { ...state, ...action.user };
        default:
            return state;
    }
}